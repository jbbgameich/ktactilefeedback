/****************************************************************************
**
** SPDX-FileCopyrightText: 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtFeedback module of the Qt Toolkit.
**
** SPDX-License-Identifier: LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KFQF-Accepted-GPL OR LicenseRef-Qt-Commercial
**
****************************************************************************/

#include <QtQml/QQmlExtensionPlugin>

#include "qdeclarativehapticseffect_p.h"
#include "qdeclarativefileeffect_p.h"
#include "qdeclarativethemeeffect_p.h"
#include "qdeclarativefeedbackeffect_p.h"
#include "qdeclarativefeedbackactuator_p.h"

QT_USE_NAMESPACE

class QDeclarativeFeedbackPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface" FILE "plugin.json")

public:
    void registerTypes(const char *uri) override
    {
        Q_ASSERT(QLatin1String(uri) == QLatin1String("org.kde.tactilefeedback"));

        int major = 1;
        int minor = 0;
        qmlRegisterUncreatableType<QDeclarativeFeedbackEffect>(uri, major, minor, "Feedback", QStringLiteral("this is the feedback namespace"));
        qmlRegisterUncreatableType<QDeclarativeFeedbackEffect>(uri, major, minor, "FeedbackEffect", QStringLiteral("this is the base feedback effect class"));
        qmlRegisterType<QDeclarativeFeedbackActuator>(uri, major, minor, "Actuator");
        qmlRegisterType<QDeclarativeFileEffect>(uri, major, minor, "FileEffect");
        qmlRegisterType<QDeclarativeHapticsEffect>(uri, major, minor, "HapticsEffect");
        qmlRegisterType<QDeclarativeThemeEffect>(uri, major, minor, "ThemeEffect");
        qmlRegisterSingletonType<QDeclarativeThemeEffect>(uri, major, minor, "EffectPlayer", [](QQmlEngine *, QJSEngine *) {
            return new QDeclarativeThemeEffect;
        });
    }
};

#include "plugin.moc"

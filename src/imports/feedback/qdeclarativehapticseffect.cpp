/****************************************************************************
**
** SPDX-FileCopyrightText: 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtFeedback module of the Qt Toolkit.
**
** SPDX-License-Identifier: LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KFQF-Accepted-GPL OR LicenseRef-Qt-Commercial
**
****************************************************************************/

#include "qdeclarativehapticseffect_p.h"
/*!
    \qmltype HapticsEffect
    \brief The HapticsEffect element represents a custom haptic feedback effect.
    \ingroup qml-feedback-api
    \inherits FeedbackEffect

    This class closely corresponds to the C++ \l QFeedbackHapticsEffect class.

    \snippet doc/src/snippets/declarative/declarative-feedback.qml Haptics Effect

    \sa Actuator, {QFeedbackHapticsEffect}
*/
QDeclarativeHapticsEffect::QDeclarativeHapticsEffect(QObject *parent)
    : QDeclarativeFeedbackEffect(parent), m_actuator(nullptr)
{
    d = new QFeedbackHapticsEffect(this);
    setFeedbackEffect(d);

    const QFeedbackActuator* fa = d->actuator();

    const QList<QFeedbackActuator*> actuators = QFeedbackActuator::actuators();
    for (QFeedbackActuator* actuator : actuators) {
        QDeclarativeFeedbackActuator* dfa = new QDeclarativeFeedbackActuator(this, actuator);
        if (fa && *fa == *actuator) {
            m_actuator = dfa;
        }
        m_actuators.push_back(dfa);
    }
}

void QDeclarativeHapticsEffect::setDuration(int msecs)
{
    if (msecs != d->duration()) {
        d->setDuration(msecs);
        Q_EMIT durationChanged();
    }
}

int QDeclarativeHapticsEffect::duration() const
{
    return d->duration();
}
/*!
    \qmlproperty double HapticsEffect::intensity

    The intensity of the main part of the haptics effect, from 0.0 to 1.0.
*/
void QDeclarativeHapticsEffect::setIntensity(qreal intensity)
{
    if (!qFuzzyCompare(intensity, d->intensity())) {
        d->setIntensity(intensity);
        Q_EMIT intensityChanged();
    }
}

qreal QDeclarativeHapticsEffect::intensity() const
{
    return d->intensity();
}

/*!
    \qmlproperty int HapticsEffect::attackTime

    The duration of the attack (fade-in) part of the haptics effect.
*/

void QDeclarativeHapticsEffect::setAttackTime(int msecs)
{
    if (msecs != d->attackTime()) {
        d->setAttackTime(msecs);
        Q_EMIT attackTimeChanged();
    }
}

int QDeclarativeHapticsEffect::attackTime() const
{
    return d->attackTime();
}
/*!
    \qmlproperty double HapticsEffect::attackIntensity

    The intensity of the attack (fade-in) part of the haptics effect, from 0.0 to 1.0.
*/
void QDeclarativeHapticsEffect::setAttackIntensity(qreal intensity)
{
    if (!qFuzzyCompare(intensity, d->attackIntensity())) {
        d->setAttackIntensity(intensity);
        Q_EMIT intensityChanged();
    }
}

qreal QDeclarativeHapticsEffect::attackIntensity() const
{
    return d->attackIntensity();
}
/*!
    \qmlproperty int HapticsEffect::fadeTime

    The duration of the fade-out part of the haptics effect.
*/

void QDeclarativeHapticsEffect::setFadeTime(int msecs)
{
    if (msecs != d->fadeTime()) {
        d->setFadeTime(msecs);
        Q_EMIT fadeTimeChanged();
    }
}

int QDeclarativeHapticsEffect::fadeTime() const
{
    return d->fadeTime();
}

void QDeclarativeHapticsEffect::setFadeIntensity(qreal intensity)
{
    if (!qFuzzyCompare(intensity, d->fadeIntensity())) {
        d->setFadeIntensity(intensity);
        Q_EMIT fadeIntensityChanged();
    }
}
/*!
    \qmlproperty double HapticsEffect::fadeIntensity

    The intensity of the fade-out part of the haptics effect, from 0.0 to 1.0.
*/
qreal QDeclarativeHapticsEffect::fadeIntensity() const
{
    return d->fadeIntensity();
}
/*!
    \qmlproperty int HapticsEffect::period

    The period of the haptics effect.  If the period is zero, the effect will
    not repeat.  If it is non-zero, the effect will repeat every period milliseconds.
*/

void QDeclarativeHapticsEffect::setPeriod(int msecs)
{
    if (msecs != d->period()) {
        d->setPeriod(msecs);
        Q_EMIT periodChanged();
    }
}

int QDeclarativeHapticsEffect::period() const
{
    return d->period();
}

/*!
    \qmlproperty Actuator HapticsEffect::actuator

    The actuator that is used for playing this effect.
    \sa Actuator
*/
void QDeclarativeHapticsEffect::setActuator(QDeclarativeFeedbackActuator *actuator)
{
    if (actuator != m_actuator) {
        if (!actuator
         || !m_actuator
         || !(*(actuator->feedbackActuator()) == *(m_actuator->feedbackActuator()))) {
            m_actuator = actuator;
            d->setActuator(m_actuator ? m_actuator->feedbackActuator() : nullptr);
            Q_EMIT actuatorChanged();
        }
    }
}

QDeclarativeFeedbackActuator* QDeclarativeHapticsEffect::actuator() const
{
    return m_actuator;
}
/*!
    \qmlproperty list<Actuator> HapticsEffect::availableActuators

    This property holds a list of available actuators.
    This property is read only.
    \sa Actuator
*/
QQmlListProperty<QDeclarativeFeedbackActuator> QDeclarativeHapticsEffect::availableActuators() {
    return QQmlListProperty<QDeclarativeFeedbackActuator>(this,
                                                                  nullptr,
                                                                  nullptr /*appending actuators are not allowed*/,
                                                                  actuator_count,
                                                                  actuator_at,
                                                                  nullptr /*removing actuators are not allowed*/);
}

qqmlistproperty_size_t QDeclarativeHapticsEffect::actuator_count(QQmlListProperty<QDeclarativeFeedbackActuator> *prop)
{
    return static_cast<QDeclarativeHapticsEffect*>(prop->object)->m_actuators.size();
}

QDeclarativeFeedbackActuator* QDeclarativeHapticsEffect::actuator_at(QQmlListProperty<QDeclarativeFeedbackActuator> *prop, qqmlistproperty_size_t index)
{
    return static_cast<QDeclarativeHapticsEffect*>(prop->object)->m_actuators.at(index);
}

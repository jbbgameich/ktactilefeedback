/****************************************************************************
**
** SPDX-FileCopyrightText: 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtFeedback module of the Qt Toolkit.
**
** SPDX-License-Identifier: LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KFQF-Accepted-GPL OR LicenseRef-Qt-Commercial
**
****************************************************************************/

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists for the convenience
// of Qt Feedback framework.  This header file may change from version
// to version without notice, or even be removed.
//
// We mean it.
//
//

#ifndef QDECLARATIVETHEMEEFFECT_P_H
#define QDECLARATIVETHEMEEFFECT_P_H

#include <QtQml/qqml.h>
#include <qfeedbackeffect.h>

QT_USE_NAMESPACE

// Wrapper for theme effects
class QDeclarativeThemeEffect : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool supported READ effectSupported CONSTANT)
    Q_PROPERTY(Effect effect READ effect WRITE setEffect NOTIFY effectChanged)

    Q_CLASSINFO("DefaultMethod", "play()")
    Q_CLASSINFO("OverloadedMethod", "play(Effect)")

public:
    enum Effect {
        Undefined = QFeedbackEffect::Undefined,
        Press = QFeedbackEffect::Press,
        Release = QFeedbackEffect::Release,
        PressWeak = QFeedbackEffect::PressWeak,
        ReleaseWeak = QFeedbackEffect::ReleaseWeak,
        PressStrong = QFeedbackEffect::PressStrong,
        ReleaseStrong = QFeedbackEffect::ReleaseStrong,
        DragStart = QFeedbackEffect::DragStart,
        DragDropInZone = QFeedbackEffect::DragDropInZone,
        DragDropOutOfZone = QFeedbackEffect::DragDropOutOfZone,
        DragCrossBoundary = QFeedbackEffect::DragCrossBoundary,
        Appear = QFeedbackEffect::Appear,
        Disappear = QFeedbackEffect::Disappear,
        Move = QFeedbackEffect::Move,
        NumberOfEffects = QFeedbackEffect::NumberOfEffects,
        UserEffect = QFeedbackEffect::UserEffect
    };

    Q_ENUM(Effect)

    QDeclarativeThemeEffect(QObject *parent = nullptr);
    bool effectSupported();
    void setEffect(Effect effect);
    Effect effect() const;

public Q_SLOTS:
    void play();
    void play(Effect effect);

Q_SIGNALS:
    void effectChanged();

public:
    Effect m_effect;
};

QML_DECLARE_TYPE(QDeclarativeThemeEffect)

#endif // QDECLARATIVETHEMEEFFECT_P_H

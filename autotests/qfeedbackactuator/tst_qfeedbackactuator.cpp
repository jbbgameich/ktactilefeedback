/****************************************************************************
**
** SPDX-FileCopyrightText: 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the QtFeedback module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL-EXCEPT$
** SPDX-License-Identifier: GPL-3.0-only WITH Qt-GPL-exception-1.0 OR LicenseRef-Qt-Commercial
**
** $QT_END_LICENSE$
**
****************************************************************************/

//TESTED_COMPONENT=src/feedback

#include <QtTest/QtTest>

#include <qfeedbackactuator.h>

QT_USE_NAMESPACE

class tst_QFeedbackActuator : public QObject
{
    Q_OBJECT
public:
    tst_QFeedbackActuator();
    ~tst_QFeedbackActuator() override;

public Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void init();
    void cleanup();

private Q_SLOTS:
    void enumeration();
    void setEnabled();
};

tst_QFeedbackActuator::tst_QFeedbackActuator()
{
}

tst_QFeedbackActuator::~tst_QFeedbackActuator()
{
}

void tst_QFeedbackActuator::initTestCase()
{
}

void tst_QFeedbackActuator::cleanupTestCase()
{
}

void tst_QFeedbackActuator::init()
{
    //the list returned should always be the same with the same order
    QCOMPARE(QFeedbackActuator::actuators(), QFeedbackActuator::actuators());
}

void tst_QFeedbackActuator::cleanup()
{
}

void tst_QFeedbackActuator::enumeration()
{
    const QList<QFeedbackActuator*> actuators = QFeedbackActuator::actuators();
#ifdef HAVE_ACTUATORS
    QVERIFY(!actuators.isEmpty());
#endif
    for (QFeedbackActuator* actuator : actuators) {
        if (actuator->name() == QStringLiteral("test plugin") || actuator->name() == QStringLiteral("5555"))
            continue;

        QVERIFY(actuator->isValid());
        QVERIFY(actuator->id() >= 0);

        // values from the hfd-service backend
        QCOMPARE(actuator->isCapabilitySupported(QFeedbackActuator::Envelope), true);
        QCOMPARE(actuator->isCapabilitySupported(QFeedbackActuator::Period), false);
        QVERIFY(!actuator->name().isEmpty());
    }

    // Try comparisons
    if (actuators.count() > 1) {
        QFeedbackActuator* a1 = actuators.at(0);
        QFeedbackActuator* a2 = actuators.at(1);
        QFeedbackActuator* a1b = actuators.at(0);
        QFeedbackActuator* a2b = actuators.at(1);
        QVERIFY(a1->id() != a2->id());
//        QVERIFY(*a1 != *a2); // doesn't work, no operator != !!
        QVERIFY(!(*a1 == *a2));

        QVERIFY(*a1 == *a1b);
        QVERIFY(*a2 == *a2b);
    }
}

void tst_QFeedbackActuator::setEnabled()
{
    const auto actuators = QFeedbackActuator::actuators();
    for (QFeedbackActuator* actuator : actuators) {
        if (actuator->name() == QStringLiteral("test plugin") || actuator->name() == QStringLiteral("5555"))
            continue;
        //this test might not always be true because you ight not be allowed to change the enabled property
        actuator->setEnabled(false);
        QVERIFY(!actuator->isEnabled());
        actuator->setEnabled(true);
        QVERIFY(actuator->isEnabled());
    }
}




QTEST_MAIN(tst_QFeedbackActuator)

#include "tst_qfeedbackactuator.moc"

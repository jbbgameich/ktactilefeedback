// SPDX-FileCopyrightText: 2017 The Qt Company Ltd.
//
// SPDX-License-Identifier: BSD-3-Clause

#ifndef HAPTICBUTTON_H
#define HAPTICBUTTON_H

#include <QWidget>

class HapticButton : public QWidget
{
    Q_OBJECT
public:
    explicit HapticButton(const QString &label);
    void setLabel(const QString& label);

protected:
    void mousePressEvent(QMouseEvent *e);
    void paintEvent(QPaintEvent *e);

private:
    QString m_label;

signals:
    void clicked();
};

#endif // HAPTICBUTTON_H

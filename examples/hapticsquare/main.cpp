// SPDX-FileCopyrightText: 2017 The Qt Company Ltd.
//
// SPDX-License-Identifier: BSD-3-Clause

#include <QtGui/QApplication>
#include "hapticsquare.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.addLibraryPath("../../plugins"); // allows the plugins to be loaded
    HapticSquare w;
    w.show();

    return a.exec();
}
